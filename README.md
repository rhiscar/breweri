# breweri


This is the service layer using springboot, for the breweri challenge. I created a simple API 
without any data persistence, since was specified on the challenge that. 

The project will run on the port 8001 and have a simple swagger documentation.
You can check the pseudo documentation for the API using this URL: 

http://localhost:8001/breweri/swagger-ui.html

OBS: Serve must run locally for this URL to work.

I created a single Rest endpoint to handle all the information traffic for the 
breweri. 

I have a BreweriService for structure purposes, but the actual logic for the 
project is inside the BreweriContainer dto.


