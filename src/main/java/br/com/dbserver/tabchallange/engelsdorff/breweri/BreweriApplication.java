package br.com.dbserver.tabchallange.engelsdorff.breweri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BreweriApplication {

	public static void main(String[] args) {
		SpringApplication.run(BreweriApplication.class, args);
	}

}
