package br.com.dbserver.tabchallange.engelsdorff.breweri.dto;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains most of the logic of this application, in here it will control temperatures for the containers
 * and validate warning messages for the beers
 */
public class BeerContainer {

    public static Long KEY_ID_CONTAINER_CONTROLLER = 0L;
    private final byte MAX_BEER_CAPACITY = 20;
    private List<Beer> beerList = new ArrayList<>();
    private Long containerId;
    private float temp;
    private boolean doorOpened;
    private LocalTime lastOpened;

    public BeerContainer() {
        temp = -10;
        beerList = new ArrayList<>();
        doorOpened = false;
    }

    public Beer addBeerToContainer(Beer beer) throws Exception {
        this.openDoor();
        if (beerList.size() < MAX_BEER_CAPACITY) {
            beerList.add(beer);
            return beer;
        }
        throw new Exception("This container can't hold more beer");
    }

    public boolean openDoor() {
        if (!doorOpened) {
            doorOpened = true;
            if (lastOpened != null) {
                float secondsOpened = ChronoUnit.SECONDS.between(lastOpened, LocalTime.now());
                this.setTemp(this.temp - (secondsOpened / 1000f));
            }
            lastOpened = LocalTime.now();
        }
        return doorOpened;
    }

    public boolean closeDoor() {
        if (doorOpened) {
            doorOpened = false;
            if (lastOpened != null) {
                float secondsOpened = ChronoUnit.SECONDS.between(lastOpened, LocalTime.now());
                this.setTemp(this.temp + (secondsOpened / 100f));
            }
            this.updateBeerStatus();
        }
        return doorOpened;
    }

    private void updateBeerStatus() {
        for (Beer b : this.getBeerList()) {
            if (b.getBeerType().getMaxTemp() < this.getTemp()) {
                b.setMsg("Too hot for this beer!");
            } else if (b.getBeerType().getMinTemp() > this.getTemp()) {
                b.setMsg("Too cold for this beer!");
            } else {
                b.setMsg("");
            }
        }
    }

    /*
        I didn't want this method to be visible but, to ease my tests I made it public, if I had more time,
        I would try to let it be private and have a workaround to test the behavior for beer msg
     */
    public void setTemp(float temp) {
        if (temp >= 10) {
            this.temp = 10;
        } else if (temp <= -10) {
            this.temp = -10;
        } else {
            this.temp = temp;
        }
    }

    public List<Beer> getBeerList() {
        return beerList;
    }

    public Beer getBeer(Long beerId) {
        this.openDoor();
        return beerList.stream().filter(beer -> beer.getBeerId().equals(beerId)).findAny().orElse(null);
    }

    public float getTemp() {
        return this.temp;
    }

    public Long getContainerId() {
        return containerId;
    }

    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }

    public boolean getDoorOpened() {
        return this.doorOpened;
    }
}
