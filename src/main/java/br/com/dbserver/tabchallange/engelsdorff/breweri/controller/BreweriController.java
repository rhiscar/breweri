package br.com.dbserver.tabchallange.engelsdorff.breweri.controller;

import br.com.dbserver.tabchallange.engelsdorff.breweri.converter.JsonConverter;
import br.com.dbserver.tabchallange.engelsdorff.breweri.dto.Beer;
import br.com.dbserver.tabchallange.engelsdorff.breweri.service.BreweriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/beer")
@CrossOrigin(origins = "*")
public class BreweriController {

    @Autowired
    BreweriService breweriService;

    private final String jsonErrorMessage = "{errorMsg: '%s'}";

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createContainer() {
        try {
            return JsonConverter.objToJson(breweriService.createBeerContainer());
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String getAllContainers() {
        try {
            return JsonConverter.objToJson(breweriService.getAllContainers());
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/{containerId}/create", method = RequestMethod.POST)
    public String createBeer (@PathVariable("containerId") Long containerId, @RequestBody Beer beer){
        try {
            return JsonConverter.objToJson(breweriService.createBeer(containerId, beer));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/{containerId}/all", method = RequestMethod.GET)
    public String getAllBeersFromContainer (@PathVariable("containerId") Long containerId){
        try {
            return JsonConverter.objToJson(breweriService.getAllBeersFromContainer(containerId));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }


    @RequestMapping(value = "/{containerId}/{beerId}", method = RequestMethod.GET)
    public String getBeer(@PathVariable("containerId") Long containerId, @PathVariable("beerId") Long beerId) {
        try {
            return JsonConverter.objToJson(breweriService.getBeer(containerId, beerId));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/{containerId}/{beerId}", method = RequestMethod.DELETE)
    public String removeBeer(@PathVariable("containerId") Long containerId, @PathVariable("beerId") Long beerId) {
        try {
            return JsonConverter.objToJson(breweriService.removeBeer(containerId, beerId));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/{containerId}/update", method = RequestMethod.PUT)
    public String updateBeer(@PathVariable("containerId") Long containerId,@RequestBody Beer beer) {
        try {
            return JsonConverter.objToJson(breweriService.updateBeer(containerId, beer));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/{containerId}/openDoor", method = RequestMethod.PUT)
    public String openContainerDoor(@PathVariable("containerId") Long containerId) {
        try {
            return JsonConverter.objToJson(breweriService.openContainderDoor(containerId));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

    @RequestMapping(value = "/{containerId}/closeDoor", method = RequestMethod.PUT)
    public String closeContainerDoor(@PathVariable("containerId") Long containerId) {
        try {
            return JsonConverter.objToJson(breweriService.closeContainderDoor(containerId));
        } catch (Exception exp) {
            return String.format(jsonErrorMessage, exp.getLocalizedMessage());
        }
    }

}
